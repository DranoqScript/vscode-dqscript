# DranoqScript support for [Visual Studio Code](https://code.visualstudio.com/)
![Example](https://gitlab.com/DranoqScript/vscode-dqscript/raw/master/images/example.png)

This is a fork of [QML for VSCode](https://github.com/bbenoist/vscode-qml)

## Installation

### From Marketplace
You can install the extension from [Marketplace](https://marketplace.visualstudio.com/items?itemName=DranoqSoftware.DranoqScript)

### Installing the extension Locally
Just clone the [GitLab repository](https://gitlab.com/DranoqScript/vscode-dqscript.git) under your local extensions folder:
* Windows: `%USERPROFILE%\.vscode\extensions`
* Mac / Linux: `$HOME/.vscode/extensions`
